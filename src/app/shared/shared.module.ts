import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '../core/core.module';

import { NotFoundComponent } from './not-found/not-found.component';
import { MainNavigationComponent } from './navigation/main-navigation/main-navigation.component';
import { AircrewListComponent } from './aircrew/aircrew-list/aircrew-list.component';
import { RequestListComponent } from './request/request-list/request-list.component';
import { AameListComponent } from './aame/aame-list/aame-list.component';
import { ProfilePreviewComponent } from './profile-preview/profile-preview.component';
import { RequestHistoryComponent } from './request/request-history/request-history.component';
import { AircrewRequestSwitchComponent } from './aircrew-request-switch/aircrew-request-switch.component';
import { AircrewNavigationComponent } from './navigation/aircrew-navigation/aircrew-navigation.component';
import { AircrewProfileComponent } from './aircrew/aircrew-profile/aircrew-profile.component';
import { AircrewTimelineComponent } from './aircrew/aircrew-timeline/aircrew-timeline.component';
import { AircrewMergeComponent } from './aircrew/aircrew-merge/aircrew-merge.component';
import { AameProfileComponent } from './aame/aame-profile/aame-profile.component';
import { LicensingRequestComponent } from './request/licensing-request/licensing-request.component';
import { MedicalsRequestNavigationComponent } from './navigation/medicals-request-navigation/medicals-request-navigation.component';
import { MedicalApplicationComponent } from './request/medicals-request/medical-application/medical-application.component';
import { MedicalReportComponent } from './request/medicals-request/medical-report/medical-report.component';
import { OtorhinolaryngologyReportComponent } from './request/medicals-request/otorhinolaryngology-report/otorhinolaryngology-report.component';
import { MedicalAttachmentsComponent } from './request/medicals-request/medical-attachments/medical-attachments.component';
import { OphthalmologyReportComponent } from './request/medicals-request/ophthalmology-report/ophthalmology-report.component';
import { AssessmentReportComponent } from './request/medicals-request/assessment-report/assessment-report.component';
import { AameErrorComponent } from './request/medicals-request/aame-error/aame-error.component';
import { AddAircrewComponent } from './modals/add-aircrew/add-aircrew.component';
import { AddAameComponent } from './modals/add-aame/add-aame.component';
import { AddLicenseComponent } from './modals/add-license/add-license.component';
import { AddCertificateComponent } from './modals/add-certificate/add-certificate.component';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { ListLoadingComponent } from './list-loading/list-loading.component';

@NgModule({
  declarations: [
    NotFoundComponent,
    MainNavigationComponent,
    AircrewListComponent,
    RequestListComponent,
    AameListComponent,
    ProfilePreviewComponent,
    RequestHistoryComponent,
    AircrewRequestSwitchComponent,
    AircrewNavigationComponent,
    AircrewProfileComponent,
    AircrewTimelineComponent,
    AircrewMergeComponent,
    AameProfileComponent,
    LicensingRequestComponent,
    MedicalsRequestNavigationComponent,
    MedicalApplicationComponent,
    MedicalReportComponent,
    OtorhinolaryngologyReportComponent,
    MedicalAttachmentsComponent,
    OphthalmologyReportComponent,
    AssessmentReportComponent,
    AameErrorComponent,
    AddAircrewComponent,
    AddAameComponent,
    AddLicenseComponent,
    AddCertificateComponent,
    ChangePasswordComponent,
    ListLoadingComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CoreModule
  ],
  exports: [
    NotFoundComponent,
    MainNavigationComponent,
    AircrewListComponent,
    RequestListComponent,
    AameListComponent,
    ProfilePreviewComponent,
    RequestHistoryComponent,
    AircrewRequestSwitchComponent,
    AircrewNavigationComponent,
    AircrewProfileComponent,
    AircrewTimelineComponent,
    AircrewMergeComponent,
    AameProfileComponent,
    LicensingRequestComponent,
    MedicalsRequestNavigationComponent,
    MedicalApplicationComponent,
    MedicalReportComponent,
    OtorhinolaryngologyReportComponent,
    MedicalAttachmentsComponent,
    OphthalmologyReportComponent,
    AssessmentReportComponent,
    AameErrorComponent,
    AddAircrewComponent,
    AddAameComponent,
    AddLicenseComponent,
    AddCertificateComponent,
    ChangePasswordComponent,
    ListLoadingComponent
  ]
})
export class SharedModule { }
