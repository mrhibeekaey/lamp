import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AameProfileComponent } from './aame-profile.component';

describe('AameProfileComponent', () => {
  let component: AameProfileComponent;
  let fixture: ComponentFixture<AameProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AameProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AameProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
