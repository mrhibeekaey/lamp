import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aame-profile',
  templateUrl: './aame-profile.component.html',
  styleUrls: ['./aame-profile.component.scss']
})
export class AameProfileComponent implements OnInit {
  aameProfile: any;

  constructor() { }

  ngOnInit() {
    this.aameProfile = {};
  }

}
