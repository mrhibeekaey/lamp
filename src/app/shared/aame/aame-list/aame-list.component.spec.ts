import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AameListComponent } from './aame-list.component';

describe('AameListComponent', () => {
  let component: AameListComponent;
  let fixture: ComponentFixture<AameListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AameListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AameListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
