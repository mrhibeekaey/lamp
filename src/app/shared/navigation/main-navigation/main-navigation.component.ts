import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {

  constructor(
    private router: Router,
    private _authService: AuthService
  ) { }

  ngOnInit() {
  }

  onLogout(event) {
    event.preventDefault();

    this._authService.logout();

    this.router.navigate(['/login']);
  }
}
