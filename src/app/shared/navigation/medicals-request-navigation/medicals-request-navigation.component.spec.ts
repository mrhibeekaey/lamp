import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalsRequestNavigationComponent } from './medicals-request-navigation.component';

describe('MedicalsRequestNavigationComponent', () => {
  let component: MedicalsRequestNavigationComponent;
  let fixture: ComponentFixture<MedicalsRequestNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalsRequestNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalsRequestNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
