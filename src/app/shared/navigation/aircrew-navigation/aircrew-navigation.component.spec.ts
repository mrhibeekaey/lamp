import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewNavigationComponent } from './aircrew-navigation.component';

describe('AircrewNavigationComponent', () => {
  let component: AircrewNavigationComponent;
  let fixture: ComponentFixture<AircrewNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
