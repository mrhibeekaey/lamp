import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-aircrew-navigation',
  templateUrl: './aircrew-navigation.component.html',
  styleUrls: ['./aircrew-navigation.component.scss']
})
export class AircrewNavigationComponent implements OnInit {
  @Input('request-id') requestId: string;

  constructor() { }

  ngOnInit() {
  }

}
