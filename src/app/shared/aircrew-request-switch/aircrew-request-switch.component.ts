import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-aircrew-request-switch',
  templateUrl: './aircrew-request-switch.component.html',
  styleUrls: ['./aircrew-request-switch.component.scss']
})
export class AircrewRequestSwitchComponent implements OnInit {
  @Input() type: string;

  toggleList: boolean;

  constructor() { }

  ngOnInit() {
    this.toggleList = false;
  }

}
