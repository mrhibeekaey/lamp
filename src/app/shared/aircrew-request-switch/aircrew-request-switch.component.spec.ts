import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewRequestSwitchComponent } from './aircrew-request-switch.component';

describe('AircrewRequestSwitchComponent', () => {
  let component: AircrewRequestSwitchComponent;
  let fixture: ComponentFixture<AircrewRequestSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewRequestSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewRequestSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
