import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAttachmentsComponent } from './medical-attachments.component';

describe('MedicalAttachmentsComponent', () => {
  let component: MedicalAttachmentsComponent;
  let fixture: ComponentFixture<MedicalAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
