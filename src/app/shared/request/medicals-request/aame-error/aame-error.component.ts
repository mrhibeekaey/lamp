import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aame-error',
  templateUrl: './aame-error.component.html',
  styleUrls: ['./aame-error.component.scss']
})
export class AameErrorComponent implements OnInit {

  aameError = {
    MedicalApplicationId: '',
    formPK: '',
    ApplicationFormErrorsNo: '',
    ApplicationFormErrorsDetails: '',
    ApplicationFormRating: '',
    MedicalExaminationReportErrorsNo: '',
    MedicalExaminationReportErrorsDetails: '',
    MedicalExaminationReportRating: '',
    AudiogramErrorsNo: '',
    AudiogramErrorsDetails: '',
    AudiogramReportRating: '',
    OpthamologyErrorsNo: '',
    OpthamologyErrorsDetails: '',
    OphthalmologyReportRating: '',
    MedicalCertificateErrorsNo: '',
    MedicalCertificateErrorsDetails: '',
    MedicalCertificateRating: '',
    OthersErrorsNo: '',
    OthersErrorsDetails: '',
    TotalErrors: '',
    MedicalExaminationLackOfCommentsErrorsNo: '',
    MedicalExaminationLackOfCommentsErrorsDetails: '',
    MedicalExaminationLackOfCommentsErrorsComments: '',
    ReportSubmission: '',
    OfficialRemarks: '',
    MedicalAssessorId: '',
    AssessorEnteredDate: ''
  };

  constructor() { }

  ngOnInit() {
  }

}
