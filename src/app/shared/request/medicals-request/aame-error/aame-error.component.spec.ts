import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AameErrorComponent } from './aame-error.component';

describe('AameErrorComponent', () => {
  let component: AameErrorComponent;
  let fixture: ComponentFixture<AameErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AameErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AameErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
