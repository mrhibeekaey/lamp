import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalApplicationComponent } from './medical-application.component';

describe('MedicalApplicationComponent', () => {
  let component: MedicalApplicationComponent;
  let fixture: ComponentFixture<MedicalApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
