import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medical-application',
  templateUrl: './medical-application.component.html',
  styleUrls: ['./medical-application.component.scss']
})
export class MedicalApplicationComponent implements OnInit {
  medicalApplication: any;

  constructor() { }

  ngOnInit() {
    this.medicalApplication = {};
  }

}
