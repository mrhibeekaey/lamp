import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtorhinolaryngologyReportComponent } from './otorhinolaryngology-report.component';

describe('OtorhinolaryngologyReportComponent', () => {
  let component: OtorhinolaryngologyReportComponent;
  let fixture: ComponentFixture<OtorhinolaryngologyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtorhinolaryngologyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtorhinolaryngologyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
