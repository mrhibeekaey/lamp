import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OphthalmologyReportComponent } from './ophthalmology-report.component';

describe('OphthalmologyReportComponent', () => {
  let component: OphthalmologyReportComponent;
  let fixture: ComponentFixture<OphthalmologyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OphthalmologyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OphthalmologyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
