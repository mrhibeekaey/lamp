import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assessment-report',
  templateUrl: './assessment-report.component.html',
  styleUrls: ['./assessment-report.component.scss']
})
export class AssessmentReportComponent implements OnInit {

  assessmentReport = {
    MedicalApplicationId: '',
    type_of_medical_assessment: '',
    aeromedical_disposition: '',
    license_type: '',
    age: '',
    date_of_medical: '',
    license_number: '',
    medical_assessor_comment: '',
    follow_up_plan: '',
    medical_assessor: '',
    AssessDate: ''
  };

  constructor() { }

  ngOnInit() {
  }

}
