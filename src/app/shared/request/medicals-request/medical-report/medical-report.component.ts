import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medical-report',
  templateUrl: './medical-report.component.html',
  styleUrls: ['./medical-report.component.scss']
})
export class MedicalReportComponent implements OnInit {
  medicalReport: any;

  constructor() { }

  ngOnInit() {
    this.medicalReport = {};
  }

}
