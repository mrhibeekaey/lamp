import { Component, OnInit, Input } from '@angular/core';

import { Request } from '../../../core/request/request';

import { RequestService } from '../../../core/request/request.service';
import { AuthService } from '../../../core/auth/auth.service';

import { URLS } from '../../../app-constants';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent extends Request implements OnInit {
  @Input() type: string;

  IMAGE_URL: string;

  loading: boolean;
  pending: any;
  archived: any;
  closed: any;
  denied: any;

  constructor(
    public _requestService: RequestService,
    public _authService: AuthService
  ) {
    super(_requestService, _authService);

    this.IMAGE_URL = URLS.image;
  }

  async ngOnInit() {
    try {
      this.loading = true;

      if (this.type === 'licensing') {
        this.pending = (await this.getPendingLicensing()).response;
      } else {
        this.pending = (await this.getPendingMedicals()).response;
      }

      this.loading = false;
    } catch (err) {
      console.error(err);
    }
  }

  getAssignedStatus(request: any): boolean {
    if (this.type === 'licensing') {
      return request.AssignedLicensingID ? true : false;
    } else {
      return request.AssignedMedicalID ? true : false;
    }
  }
}
