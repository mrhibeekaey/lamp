import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicensingRequestComponent } from './licensing-request.component';

describe('LicensingRequestComponent', () => {
  let component: LicensingRequestComponent;
  let fixture: ComponentFixture<LicensingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicensingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicensingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
