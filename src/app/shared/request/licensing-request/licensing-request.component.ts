import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-licensing-request',
  templateUrl: './licensing-request.component.html',
  styleUrls: ['./licensing-request.component.scss']
})
export class LicensingRequestComponent implements OnInit {
  licensingRequest: any;
  licensingRequestId: string;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.licensingRequestId = params.get('requestId');
    });

    this.licensingRequest = {};
  }

}
