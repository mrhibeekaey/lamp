import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAircrewComponent } from './add-aircrew.component';

describe('AddAircrewComponent', () => {
  let component: AddAircrewComponent;
  let fixture: ComponentFixture<AddAircrewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAircrewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAircrewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
