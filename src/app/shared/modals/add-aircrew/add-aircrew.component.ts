import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-aircrew',
  templateUrl: './add-aircrew.component.html',
  styleUrls: ['./add-aircrew.component.scss']
})
export class AddAircrewComponent implements OnInit {
  addAircrew: any;

  constructor() { }

  ngOnInit() {
    this.addAircrew = {};
  }

}
