import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-certificate',
  templateUrl: './add-certificate.component.html',
  styleUrls: ['./add-certificate.component.scss']
})
export class AddCertificateComponent implements OnInit {
  addCertificate: any;

  constructor() { }

  ngOnInit() {
    this.addCertificate = {};
  }

}
