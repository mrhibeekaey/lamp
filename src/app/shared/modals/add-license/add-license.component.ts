import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-license',
  templateUrl: './add-license.component.html',
  styleUrls: ['./add-license.component.scss']
})
export class AddLicenseComponent implements OnInit {
  addLicense: any;

  constructor() { }

  ngOnInit() {
    this.addLicense = {};
  }

}
