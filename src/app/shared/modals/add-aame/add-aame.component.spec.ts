import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAameComponent } from './add-aame.component';

describe('AddAameComponent', () => {
  let component: AddAameComponent;
  let fixture: ComponentFixture<AddAameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
