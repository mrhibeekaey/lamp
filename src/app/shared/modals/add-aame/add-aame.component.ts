import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-aame',
  templateUrl: './add-aame.component.html',
  styleUrls: ['./add-aame.component.scss']
})
export class AddAameComponent implements OnInit {
  addAame: any;

  constructor() { }

  ngOnInit() {
    this.addAame = {};
  }

}
