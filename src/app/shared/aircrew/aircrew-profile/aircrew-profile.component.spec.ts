import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewProfileComponent } from './aircrew-profile.component';

describe('AircrewProfileComponent', () => {
  let component: AircrewProfileComponent;
  let fixture: ComponentFixture<AircrewProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
