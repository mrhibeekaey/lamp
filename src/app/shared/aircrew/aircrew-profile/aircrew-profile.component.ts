import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aircrew-profile',
  templateUrl: './aircrew-profile.component.html',
  styleUrls: ['./aircrew-profile.component.scss']
})
export class AircrewProfileComponent implements OnInit {
  aircrewProfile: any;

  constructor() { }

  ngOnInit() {
    this.aircrewProfile = {};
  }

}
