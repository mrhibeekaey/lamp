import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewTimelineComponent } from './aircrew-timeline.component';

describe('AircrewTimelineComponent', () => {
  let component: AircrewTimelineComponent;
  let fixture: ComponentFixture<AircrewTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
