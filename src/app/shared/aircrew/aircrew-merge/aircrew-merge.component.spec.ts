import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewMergeComponent } from './aircrew-merge.component';

describe('AircrewMergeComponent', () => {
  let component: AircrewMergeComponent;
  let fixture: ComponentFixture<AircrewMergeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewMergeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewMergeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
