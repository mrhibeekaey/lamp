import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircrewListComponent } from './aircrew-list.component';

describe('AircrewListComponent', () => {
  let component: AircrewListComponent;
  let fixture: ComponentFixture<AircrewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircrewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircrewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
