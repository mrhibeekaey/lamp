import { Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';

import { AircrewService } from '../../../core/aircrew/aircrew.service';
import { AuthService } from '../../../core/auth/auth.service';

import { URLS } from '../../../app-constants';

@Component({
  selector: 'app-aircrew-list',
  templateUrl: './aircrew-list.component.html',
  styleUrls: ['./aircrew-list.component.scss']
})
export class AircrewListComponent implements OnInit {
  IMAGE_URL: string;

  loading: boolean;
  aircrews: any;

  constructor(
    private _aircrewService: AircrewService,
    private _authService: AuthService
  ) {
    this.IMAGE_URL = URLS.image;
  }

  async ngOnInit() {
    try {
      this.loading = true;

      this.aircrews = (await this.getAircrews()).all_aircrew;

      this.loading = false;
    } catch (err) {
      console.error(err);
    }
  }

  getAircrews(page: number = 1): Promise<any> {
    const payload = {
      token: this._authService.token,
      pagination: page
    };

    return new Promise((resolve, reject) => {
      this._aircrewService.aircrews(payload).subscribe(
        (data: any) => {
          resolve(data);
        },
        error => {
          reject(throwError(error));
        }
      )
    });
  }
}
