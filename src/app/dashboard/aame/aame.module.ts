import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

import { AamePersonnelModule } from './aame-personnel/aame-personnel.module';

import { AameRoutingModule } from './aame-routing.module';
import { AameComponent } from './aame.component';
import { AameHomeComponent } from './aame-home/aame-home.component';

@NgModule({
  declarations: [
    AameComponent,
    AameHomeComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    AameRoutingModule,
    AamePersonnelModule
  ],
  exports: [AameComponent]
})
export class AameModule { }
