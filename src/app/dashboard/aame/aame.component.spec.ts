import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AameComponent } from './aame.component';

describe('AameComponent', () => {
  let component: AameComponent;
  let fixture: ComponentFixture<AameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
