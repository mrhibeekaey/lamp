import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AameHomeComponent } from './aame-home/aame-home.component';
import { AamePersonnelComponent } from './aame-personnel/aame-personnel.component';

const routes: Routes = [
  { path: '', component: AameHomeComponent },
  { path: ':id', component: AamePersonnelComponent, loadChildren: () => import('./aame-personnel/aame-personnel.module').then(mod => mod.AamePersonnelModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AameRoutingModule { }
