import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AamePersonnelComponent } from './aame-personnel.component';

describe('AamePersonnelComponent', () => {
  let component: AamePersonnelComponent;
  let fixture: ComponentFixture<AamePersonnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AamePersonnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AamePersonnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
