import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-aame-personnel',
  templateUrl: './aame-personnel.component.html',
  styleUrls: ['./aame-personnel.component.scss']
})
export class AamePersonnelComponent implements OnInit {
  personnel: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.personnel = {
        profile: {
          id: params.get('id')
        }
      };
    });
  }

}
