import { NgModule } from '@angular/core';
import { CoreModule } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';

import { AamePersonnelRoutingModule } from './aame-personnel-routing.module';
import { AamePersonnelComponent } from './aame-personnel.component';

@NgModule({
  declarations: [AamePersonnelComponent],
  imports: [
    CoreModule,
    SharedModule,
    AamePersonnelRoutingModule
  ]
})
export class AamePersonnelModule { }
