import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AameHomeComponent } from './aame-home.component';

describe('AameHomeComponent', () => {
  let component: AameHomeComponent;
  let fixture: ComponentFixture<AameHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AameHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AameHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
