import { Title }  from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';

import { LicensingModule } from './licensing/licensing.module';
import { MedicalsModule } from './medicals/medicals.module';
import { AameModule } from './aame/aame.module';
import { ReportsModule } from './reports/reports.module';
import { AdministrationModule } from './administration/administration.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CoreModule,
    SharedModule,
    DashboardRoutingModule,
    LicensingModule,
    MedicalsModule,
    AameModule,
    ReportsModule,
    AdministrationModule
  ],
  providers: [Title],
  exports: [DashboardComponent]
})
export class DashboardModule { }
