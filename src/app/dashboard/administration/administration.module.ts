import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

import { AdministrationRoutingModule } from './administration-routing.module';
import { AdministrationComponent } from './administration.component';

@NgModule({
  declarations: [AdministrationComponent],
  imports: [
    CoreModule,
    SharedModule,
    AdministrationRoutingModule
  ],
  exports: [AdministrationComponent]
})
export class AdministrationModule { }
