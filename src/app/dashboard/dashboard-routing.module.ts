import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicensingComponent } from './licensing/licensing.component';
import { MedicalsComponent } from './medicals/medicals.component';
import { AameComponent } from './aame/aame.component';
import { ReportsComponent } from './reports/reports.component';
import { AdministrationComponent } from './administration/administration.component';
import { NotFoundComponent } from '../shared/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'licensing', pathMatch: 'full' },
  { path: 'licensing', component: LicensingComponent, loadChildren: () => import('./licensing/licensing.module').then(mod => mod.LicensingModule) },
  { path: 'medicals', component: MedicalsComponent, loadChildren: () => import('./medicals/medicals.module').then(mod => mod.MedicalsModule) },
  { path: 'aame', component: AameComponent, loadChildren: () => import('./aame/aame.module').then(mod => mod.AameModule) },
  { path: 'reports', component: ReportsComponent, loadChildren: () => import('./reports/reports.module').then(mod => mod.ReportsModule) },
  { path: 'administration', component: AdministrationComponent, loadChildren: () => import('./administration/administration.module').then(mod => mod.AdministrationModule) },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
