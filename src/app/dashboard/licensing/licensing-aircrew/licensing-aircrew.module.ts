import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';

import { LicensingAircrewRoutingModule } from './licensing-aircrew-routing.module';
import { LicensingAircrewComponent } from './licensing-aircrew.component';

@NgModule({
  declarations: [LicensingAircrewComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CoreModule,
    SharedModule,
    LicensingAircrewRoutingModule
  ]
})
export class LicensingAircrewModule { }
