import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-licensing-aircrew',
  templateUrl: './licensing-aircrew.component.html',
  styleUrls: ['./licensing-aircrew.component.scss']
})
export class LicensingAircrewComponent implements OnInit {
  aircrew: any;
  aircrewId: string;
  requestId: string;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.requestId = '0';
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.aircrewId = params.get('id');
    });
  }

}
