import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicensingRequestComponent } from '../../../shared/request/licensing-request/licensing-request.component';
import { AircrewProfileComponent } from '../../../shared/aircrew/aircrew-profile/aircrew-profile.component';
import { AircrewTimelineComponent } from '../../../shared/aircrew/aircrew-timeline/aircrew-timeline.component';
import { AircrewMergeComponent } from '../../../shared/aircrew/aircrew-merge/aircrew-merge.component';

const routes: Routes = [
  { path: '', redirectTo: 'profile', pathMatch: 'full' },
  { path: 'request/:requestId', component: LicensingRequestComponent },
  { path: 'profile', component: AircrewProfileComponent },
  { path: 'timeline', component: AircrewTimelineComponent },
  { path: 'merge', component: AircrewMergeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LicensingAircrewRoutingModule { }
