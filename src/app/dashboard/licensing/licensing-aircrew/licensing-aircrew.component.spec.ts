import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicensingAircrewComponent } from './licensing-aircrew.component';

describe('LicensingAircrewComponent', () => {
  let component: LicensingAircrewComponent;
  let fixture: ComponentFixture<LicensingAircrewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicensingAircrewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicensingAircrewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
