import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

import { LicensingAircrewModule } from './licensing-aircrew/licensing-aircrew.module';

import { LicensingRoutingModule } from './licensing-routing.module';
import { LicensingComponent } from './licensing.component';
import { LicensingHomeComponent } from './licensing-home/licensing-home.component';

@NgModule({
  declarations: [
    LicensingComponent,
    LicensingHomeComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    LicensingRoutingModule,
    LicensingAircrewModule
  ],
  exports: [LicensingComponent]
})
export class LicensingModule { }
