import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicensingHomeComponent } from './licensing-home/licensing-home.component';
import { LicensingAircrewComponent } from './licensing-aircrew/licensing-aircrew.component';

const routes: Routes = [
  { path: '', component: LicensingHomeComponent },
  { path: ':id', component: LicensingAircrewComponent, loadChildren: () => import('./licensing-aircrew/licensing-aircrew.module').then(mod => mod.LicensingAircrewModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LicensingRoutingModule { }
