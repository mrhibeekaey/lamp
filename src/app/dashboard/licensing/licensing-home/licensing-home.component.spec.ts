import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicensingHomeComponent } from './licensing-home.component';

describe('LicensingHomeComponent', () => {
  let component: LicensingHomeComponent;
  let fixture: ComponentFixture<LicensingHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicensingHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicensingHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
