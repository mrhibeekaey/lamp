import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';

@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CoreModule,
    SharedModule,
    ReportsRoutingModule
  ],
  exports: [ReportsComponent]
})
export class ReportsModule { }
