import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalsHomeComponent } from './medicals-home.component';

describe('MedicalsHomeComponent', () => {
  let component: MedicalsHomeComponent;
  let fixture: ComponentFixture<MedicalsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
