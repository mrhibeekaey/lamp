import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalsHomeComponent } from './medicals-home/medicals-home.component';
import { MedicalsAircrewComponent } from './medicals-aircrew/medicals-aircrew.component';

const routes: Routes = [
  { path: '', component: MedicalsHomeComponent },
  { path: ':id', component: MedicalsAircrewComponent, loadChildren: () => import('./medicals-aircrew/medicals-aircrew.module').then(mod => mod.MedicalsAircrewModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicalsRoutingModule { }
