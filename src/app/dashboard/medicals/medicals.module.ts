import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

import { MedicalsAircrewModule } from './medicals-aircrew/medicals-aircrew.module';

import { MedicalsRoutingModule } from './medicals-routing.module';
import { MedicalsComponent } from './medicals.component';
import { MedicalsHomeComponent } from './medicals-home/medicals-home.component';

@NgModule({
  declarations: [
    MedicalsComponent,
    MedicalsHomeComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    MedicalsRoutingModule,
    MedicalsAircrewModule
  ],
  exports: [MedicalsComponent]
})
export class MedicalsModule { }
