import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalsAircrewComponent } from './medicals-aircrew.component';

describe('MedicalsAircrewComponent', () => {
  let component: MedicalsAircrewComponent;
  let fixture: ComponentFixture<MedicalsAircrewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalsAircrewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalsAircrewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
