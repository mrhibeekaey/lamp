import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-medicals-aircrew',
  templateUrl: './medicals-aircrew.component.html',
  styleUrls: ['./medicals-aircrew.component.scss']
})
export class MedicalsAircrewComponent implements OnInit {
  aircrew: any;
  aircrewId: string;
  requestId: string;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.aircrewId = params.get('id');
      this.requestId = '0';
    });
  }

}
