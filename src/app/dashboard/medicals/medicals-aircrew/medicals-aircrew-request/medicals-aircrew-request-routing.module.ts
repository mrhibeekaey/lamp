import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalApplicationComponent } from '../../../../shared/request/medicals-request/medical-application/medical-application.component';
import { MedicalReportComponent } from '../../../../shared/request/medicals-request/medical-report/medical-report.component';
import { OtorhinolaryngologyReportComponent } from '../../../../shared/request/medicals-request/otorhinolaryngology-report/otorhinolaryngology-report.component';
import { MedicalAttachmentsComponent } from '../../../../shared/request/medicals-request/medical-attachments/medical-attachments.component';
import { OphthalmologyReportComponent } from '../../../../shared/request/medicals-request/ophthalmology-report/ophthalmology-report.component';
import { AssessmentReportComponent } from '../../../../shared/request/medicals-request/assessment-report/assessment-report.component';
import { AameErrorComponent } from '../../../../shared/request/medicals-request/aame-error/aame-error.component';

const routes: Routes = [
  { path: '', redirectTo: 'medical-application', pathMatch: 'full' },
  { path: 'medical-application', component: MedicalApplicationComponent },
  { path: 'medical-report', component: MedicalReportComponent },
  { path: 'otorhinolaryngology-report', component: OtorhinolaryngologyReportComponent },
  { path: 'medical-attachments', component: MedicalAttachmentsComponent },
  { path: 'ophthalmology-report', component: OphthalmologyReportComponent },
  { path: 'assessment-report', component: AssessmentReportComponent },
  { path: 'aame-error', component: AameErrorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicalsAircrewRequestRoutingModule { }
