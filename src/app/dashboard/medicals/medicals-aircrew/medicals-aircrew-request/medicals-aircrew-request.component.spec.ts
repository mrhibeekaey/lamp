import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalsAircrewRequestComponent } from './medicals-aircrew-request.component';

describe('MedicalsAircrewRequestComponent', () => {
  let component: MedicalsAircrewRequestComponent;
  let fixture: ComponentFixture<MedicalsAircrewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalsAircrewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalsAircrewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
