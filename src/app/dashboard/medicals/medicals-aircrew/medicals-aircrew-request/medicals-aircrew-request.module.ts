import { NgModule } from '@angular/core';
import { CoreModule } from '../../../../core/core.module';
import { SharedModule } from '../../../../shared/shared.module';

import { MedicalsAircrewRequestRoutingModule } from './medicals-aircrew-request-routing.module';
import { MedicalsAircrewRequestComponent } from './medicals-aircrew-request.component';

@NgModule({
  declarations: [MedicalsAircrewRequestComponent],
  imports: [
    CoreModule,
    SharedModule,
    MedicalsAircrewRequestRoutingModule
  ],
  exports: [MedicalsAircrewRequestComponent]
})
export class MedicalsAircrewRequestModule { }
