import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';

import { MedicalsAircrewRequestModule } from './medicals-aircrew-request/medicals-aircrew-request.module'

import { MedicalsAircrewRoutingModule } from './medicals-aircrew-routing.module';
import { MedicalsAircrewComponent } from './medicals-aircrew.component';

@NgModule({
  declarations: [MedicalsAircrewComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CoreModule,
    SharedModule,
    MedicalsAircrewRoutingModule,
    MedicalsAircrewRequestModule
  ],
  exports: [MedicalsAircrewComponent]
})
export class MedicalsAircrewModule { }
