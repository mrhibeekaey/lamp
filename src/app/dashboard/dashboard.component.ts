import { Title }  from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private titleService: Title
  ) {
    this.titleService.setTitle('Dashboard | LAMP');
  }

  ngOnInit() {
  }

}
