import { OptionsService } from './options.service';

import { Aircrew } from './aircrew';
import { Aame } from './aame';
import { Licensing } from './licensing';
import { Medicals } from './medicals';

export class Options implements Aircrew, Aame, Licensing, Medicals {

  constructor(
    public _optionsService: OptionsService
  ) { }

}
applyMixins(Options, [Aircrew, Aame, Licensing, Medicals]);

function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}