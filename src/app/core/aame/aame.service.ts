import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ENDPOINTS } from '../../app-constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AameService {

  constructor(
    private http: HttpClient
  ) { }

  aames(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}allaames`, data, httpOptions);
  }

  searchAame(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}aame`, data, httpOptions);
  }

  getAame(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}aamedetails`, data, httpOptions);
  }

  saveAame(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}saveaame`, data, httpOptions);
  }
}
