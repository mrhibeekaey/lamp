import { Request } from './request/request';
import { Options } from './options/options';

import { RequestService } from './request/request.service';
import { OptionsService } from './options/options.service';
import { AuthService } from './auth/auth.service';

export class RequestOptions implements Request, Options {

  constructor(
    public _requestService: RequestService,
    public _optionsService: OptionsService,
    public _authService: AuthService
  ) { }

  getPendingLicensing: (page?: number) => Promise<any>;

  getPendingMedicals: (page?: number) => Promise<any>;
}
applyMixins(RequestOptions, [Request, Options]);

function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}