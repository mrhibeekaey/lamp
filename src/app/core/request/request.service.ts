import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ENDPOINTS } from '../../app-constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private http: HttpClient
  ) { }

  pendingLicensing(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}pending`, data, httpOptions);
  }

  archivedLicensing(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}suspended`, data, httpOptions);
  }

  closedLicensing(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}closed`, data, httpOptions);
  }

  deniedLicensing(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}denied`, data, httpOptions);
  }

  pendingMedicals(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}pendingmedicalrequests`, data, httpOptions);
  }

  archivedMedicals(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}suspendedmedicalrequests`, data, httpOptions);
  }

  closedMedicals(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}closedmedicalrequests`, data, httpOptions);
  }

  deniedMedicals(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}deniedmedicalrequests`, data, httpOptions);
  }
}
