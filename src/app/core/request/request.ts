import { RequestService } from './request.service';
import { AuthService } from '../../core/auth/auth.service';

import { Licensing } from './licensing';
import { Medicals } from './medicals';

export class Request implements Licensing, Medicals {

  constructor(
    public _requestService: RequestService,
    public _authService: AuthService
  ) { }

  getPendingLicensing: (page?: number) => Promise<any>;

  getPendingMedicals: (page?: number) => Promise<any>;
}
applyMixins(Request, [Licensing, Medicals]);

function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}