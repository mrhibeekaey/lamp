import { throwError } from 'rxjs';

import { RequestService } from './request.service';
import { AuthService } from '../../core/auth/auth.service';

export class Licensing {

  constructor(
    public _requestService: RequestService,
    public _authService: AuthService
  ) { }

  getPendingLicensing(page: number = 1): Promise<any> {
    const payload = {
      token: this._authService.token,
      pagination: page
    };

    return new Promise((resolve, reject) => {
      this._requestService.pendingLicensing(payload).subscribe(
        (data: any) => {
          resolve(data);
        },
        error => {
          reject(throwError(error));
        }
      );
    });
  }
}
