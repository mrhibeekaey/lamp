import { throwError } from 'rxjs';

import { RequestService } from './request.service';
import { AuthService } from '../../core/auth/auth.service';

export class Medicals {

  constructor(
    public _requestService: RequestService,
    public _authService: AuthService
  ) { }

  getPendingMedicals(page: number = 1): Promise<any> {
    const payload = {
      token: this._authService.token,
      pagination: page
    };

    return new Promise((resolve, reject) => {
      this._requestService.pendingMedicals(payload).subscribe(
        (data: any) => {
          resolve(data);
        },
        error => {
          reject(throwError(error));
        }
      );
    });
  }
}
