import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StorageServiceModule } from 'ngx-webstorage-service';

import { ErrorHandlerService } from './error-handler/error-handler.service';
import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';
import { OptionsService } from './options/options.service';
import { AircrewService } from './aircrew/aircrew.service';
import { RequestService } from './request/request.service';
import { AameService } from './aame/aame.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StorageServiceModule
  ],
  providers: [
    ErrorHandlerService,
    AuthService,
    UserService,
    OptionsService,
    AircrewService,
    RequestService,
    AameService
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CoreModule { }
