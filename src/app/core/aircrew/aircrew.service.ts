import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ENDPOINTS } from '../../app-constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AircrewService {

  constructor(
    private http: HttpClient
  ) { }

  aircrews(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}allaircrew`, data, httpOptions);
  }

  searchAircrew(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}aircrew`, data, httpOptions);
  }

  getAircrew(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}aircrewdetails`, data, httpOptions);
  }

  saveAircrew(data: any): Observable<any> {
    return this.http.post(`${ENDPOINTS[1]}saveaircrew`, data, httpOptions);
  }
}
