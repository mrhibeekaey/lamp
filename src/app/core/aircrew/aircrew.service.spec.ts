import { TestBed } from '@angular/core/testing';

import { AircrewService } from './aircrew.service';

describe('AircrewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AircrewService = TestBed.get(AircrewService);
    expect(service).toBeTruthy();
  });
});
