import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { throwError } from 'rxjs';

import { ENDPOINTS } from '../../app-constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const USER_STORAGE_KEY = 'user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http: HttpClient
  ) { }

  login(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${ENDPOINTS[1]}admin`, data, httpOptions).subscribe(
        (data: any) => {
          this.storage.set(USER_STORAGE_KEY, {
            token: data.access_token,
            user: data.user
          });

          resolve(data);
        },
        error => {
          reject(throwError(error));
        }
      )
    });
  }

  isAuthenticated(): boolean {
    // return this.storage.has(USER_STORAGE_KEY);
    return true;
  }

  get token() {
    return this.storage.get(USER_STORAGE_KEY).token;
  }

  logout() {
    this.storage.remove(USER_STORAGE_KEY);
  }
}
