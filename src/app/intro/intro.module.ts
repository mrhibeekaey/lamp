import { Title }  from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';

import { IntroRoutingModule } from './intro-routing.module';
import { IntroComponent } from './intro.component';

@NgModule({
  declarations: [IntroComponent],
  imports: [
    CoreModule,
    IntroRoutingModule
  ],
  providers: [Title],
  exports: [IntroComponent]
})
export class IntroModule { }
