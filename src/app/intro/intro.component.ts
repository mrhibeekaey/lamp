import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../core/auth/auth.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  intro: any;
  loading: boolean;

  constructor(
    private titleService: Title,
    private router: Router,
    private _authService: AuthService
  ) {
    this.titleService.setTitle('Login | LAMP');
  }

  ngOnInit() {
    this.intro = {};

    this.loading = false;
  }

  async onSubmit() {
    if (!this.loading) {
      try {
        this.loading = true;

        if (await this._authService.login(this.intro)) {
          this.router.navigate(['/dashboard']);
        }

        this.loading = false;
      } catch (err) {
        this.loading = false;

        console.error(err);
      }
    }
  }
}
